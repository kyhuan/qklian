package qming;


import utils.StringUtil;

public class Block {
    public String hash;
    public String previousHash;
    private String data; //数据
    private long timeStamp; //时间戳

    //区块构造函数
    public Block(String data, String previousHash) {
        this.data = data;
        this.previousHash = previousHash;
        this.timeStamp = System.currentTimeMillis();
        this.hash = calculateHash(); //确保hash值的来源
    }

    public String calculateHash() {
        String calculatedhash = StringUtil.applySha256(
                previousHash +
                        Long.toString(timeStamp) +
                        data
        );
        return calculatedhash;
    }
}
